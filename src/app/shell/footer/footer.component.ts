import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  email = 'qsmuazzam@gmail.com';

  contact = [
    {
      icon: 'fab fa-linkedin-in',
      link: 'https://www.linkedin.com/in/muazzam-syed/',
    },
    {
      icon: 'fab fa-bitbucket',
      link: 'https://bitbucket.org/SyedMuazzam/',
    },
    {
      icon: 'fab fa-github',
      link: 'https://github.com/MuazzamSyed',
    },
    {
      icon: 'fab fa-whatsapp',
      link: 'https://wa.me/919682551194?text=Hi%21%20I%20saw%20your%20website.%20Would%20like%20to%20connect.',
    },
    {
      icon: 'fab fa-instagram',
      link: 'https://instagram.com/_syed_muazzam/',
    },
  ];

  constructor() {}

  ngOnInit(): void {
    setTimeout(() => {
      document.getElementById('foot').classList.remove('init');
    }, 50);
  }
}
