import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  menuHidden = true;
  lastScrollY = 0;

  menuList = ['About Me', 'Experience', 'Projects', 'Resume'];
  bars: any;
  menu: any;
  timeOut: any;
  scroll = '0';
  h = document.documentElement;
  b = document.body;
  st = 'scrollTop';
  sh = 'scrollHeight';

  constructor() {}

  ngOnInit() {
    this.bars = document.getElementById('menu-bars');
    this.menu = document.getElementById('menu');
  }

  @HostListener('window:scroll', ['$event'])
  scrollMe = (event: Event) => {
    this.scroll =
      ((this.h[this.st] || this.b[this.st]) / ((this.h[this.sh] || this.b[this.sh]) - this.h.clientHeight)) * 100 + '%';
    // console.log(percent);
    var currentScrollPos = window.pageYOffset;
    if (this.lastScrollY > currentScrollPos) {
      document.getElementById('navbar').style.top = '0px';
    } else {
      document.getElementById('navbar').style.top = '-70px';
      this.hideMenu(0);
    }
    this.lastScrollY = currentScrollPos;
  };

  toggleMenu() {
    this.menuHidden = !this.menuHidden;
  }
  menuExp = () => {
    this.bars.classList.toggle('change');
    this.menu.classList.toggle('hidden');
    this.hideMenu(5000);
  };
  hideMenu = (time: any) => {
    clearTimeout(this.timeOut);
    if (!this.menu.classList.contains('hidden')) {
      this.timeOut = setTimeout(() => {
        this.menu.classList.add('hidden');
        this.bars.classList.remove('change');
      }, time);
    }
  };

  scrollTo(el: HTMLElement) {
    el.scrollIntoView();
  }
}
