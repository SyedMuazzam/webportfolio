import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  isLoading = false;
  message = 'Area under maintainence. Check again later maybe!';

  skills = [
    {
      img: '/assets/js.png',
      alt: 'I have extensively worked on Angular version 7 to 11',
    },
    {
      img: '/assets/ts.png',
      alt: 'I have extensively worked on Angular version 7 to 11',
    },

    {
      // img: '//logo.clearbit.com/',
      img: '/assets/html.png',
      alt: '',
    },
    {
      img: '/assets/css.png',
      alt: '',
    },
    {
      img: '/assets/angular.png',
      alt: 'I have extensively worked on Angular version 7 to 11',
    },
    {
      img: '/assets/java.png',
      alt: '',
    },
    {
      img: '/assets/node.png',
      alt: '',
    },
    {
      img: '/assets/mongodb.png',
      alt: '',
    },
    {
      img: '/assets/chrome-dev.png',
      alt: '',
    },
    {
      img: '/assets/Octocat.png',
      alt: '',
    },
    {
      img: '/assets/jquery.gif',
      alt: 'I have extensively worked on Angular version 7 to 11',
    },
    {
      img: '/assets/bootstrap.png',
      alt: '',
    },
    {
      img: '/assets/linux.png',
      alt: '',
    },
    {
      img: '/assets/flutter.png',
      alt: '',
    },
    {
      img: '/assets/firebase.png',
      alt: '',
    },
  ];

  designTools = [
    {
      img: '/assets/photoshop.png',
      alt: '',
    },
    {
      img: '/assets/illustrator.jpeg',
      alt: '',
    },
    {
      img: '/assets/ae.png',
      alt: '',
    },
    {
      img: '/assets/lr.png',
      alt: '',
    },
    {
      img: '/assets/xd.png',
      alt: '',
    },
    {
      img: '/assets/figma.png',
      alt: '',
    },
  ];

  constructor() {}

  ngOnInit() {
    this.isLoading = true;
    setTimeout(() => {
      document.getElementById('hero').classList.remove('init');
    }, 500);
  }

  scroll(el: HTMLElement) {
    el.scrollIntoView();
  }
}
