/*
 * Extra typings definitions
 */

// Allow .json files imports
declare module '*.json';
// declare module 'waypoints/lib/noframework.waypoints.min.js';
// SystemJS module definition
declare var module: NodeModule;
interface NodeModule {
  id: string;
}
